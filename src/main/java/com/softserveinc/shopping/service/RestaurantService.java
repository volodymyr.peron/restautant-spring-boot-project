package com.softserveinc.shopping.service;

import com.softserveinc.shopping.dao.RestaurantDAO;
import com.softserveinc.shopping.model.Restaurant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RestaurantService {

    private final RestaurantDAO restaurantDAO;

    @Autowired
    public RestaurantService(RestaurantDAO restaurantDAO) {
        this.restaurantDAO = restaurantDAO;
    }

    public Integer getRestaurantsCount() {
        return restaurantDAO.getRestaurantsCount();
    }

    public String getRestaurantNameById(Integer id) {
        return restaurantDAO.getRestaurantNameById(id);
    }

    public Restaurant getRestaurant(Restaurant restaurant) {
        return restaurantDAO.getRestaurant(restaurant);
    }

}
