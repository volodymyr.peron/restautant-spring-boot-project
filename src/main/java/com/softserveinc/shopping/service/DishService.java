package com.softserveinc.shopping.service;

import com.softserveinc.shopping.exception.DishNotFoundException;
import com.softserveinc.shopping.model.Dish;
import com.softserveinc.shopping.property.ShoppingProperties;
import com.softserveinc.shopping.repository.DishRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DishService {

    private final DishRepository dishRepository;
    private final ShoppingProperties shoppingProperties;

    @Autowired
    public DishService(DishRepository dishRepository, ShoppingProperties shoppingProperties) {
        this.dishRepository = dishRepository;
        this.shoppingProperties = shoppingProperties;
    }

    public List<Dish> getDishes() {
        return dishRepository.findAll();
    }

    @Transactional
    public Dish save(Dish dish) {
        return dishRepository.save(dish);
    }

    public Dish getById(Integer id) {
        return dishRepository.findById(id).orElseThrow(DishNotFoundException::new);
    }

    @Transactional
    public Dish update(Dish dish) {
        if (dishRepository.existsById(dish.getId())) {
            return dishRepository.save(dish);
        }
        throw new DishNotFoundException();
    }

    public List<Dish> getDishesByNameWithComponents(String name) {
        return dishRepository.getDishesByNameWithComponents(name);
    }

    @Transactional
    public void deleteById(Integer id) {
        dishRepository.deleteById(id);
    }

    public List<Dish> getWhereNameIsNotNullPaginated() {
        Pageable pageable = PageRequest.of(0, shoppingProperties.getPaginationSize());
        return dishRepository.findByNameIsNotNull(pageable);
    }
}
