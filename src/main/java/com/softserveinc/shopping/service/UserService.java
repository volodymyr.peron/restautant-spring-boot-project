package com.softserveinc.shopping.service;

import com.softserveinc.shopping.model.User;
import com.softserveinc.shopping.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Mono<User> findById(Long id) {
        // map to reactive type here because postgres doesn't support reactive fetching.
        return userRepository.findById(id)
                .map(Mono::just)
                .orElseGet(Mono::empty);
    }

    public Mono<User> save(Mono<User> userMono) {
        // mapping from mono to domain object as relational database doesn't support reactive operations
        return Mono.just(userRepository.save(userMono.block()));
    }

    public void saveAll(Flux<User> userFlux) {
        userFlux.subscribe(userRepository::save);
    }
}
