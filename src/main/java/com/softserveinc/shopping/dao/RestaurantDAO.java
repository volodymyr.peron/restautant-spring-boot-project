package com.softserveinc.shopping.dao;

import com.softserveinc.shopping.model.Restaurant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class RestaurantDAO {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final JdbcTemplate jdbcTemplate;
    private static final RestaurantMapper RESTAURANT_MAPPER = new RestaurantMapper();

    @Autowired
    public RestaurantDAO(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public Integer getRestaurantsCount() {
        return jdbcTemplate.queryForObject("select count(*) from restaurant", Integer.class);
    }

    public String getRestaurantNameById(Integer id) {
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("id", id);
        return namedParameterJdbcTemplate.queryForObject("select name from restaurant where id = :id", namedParameters, String.class);
    }

    public Restaurant getRestaurant(Restaurant restaurant) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(restaurant);
        return namedParameterJdbcTemplate.queryForObject("select * from restaurant where id = :id and name = :name " +
                        "and owner = :owner and address = :address",
                namedParameters, RESTAURANT_MAPPER);
    }

    static class RestaurantMapper implements RowMapper<Restaurant> {

        @Override
        public Restaurant mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Restaurant(resultSet.getInt("id"), resultSet.getString("name"),
                    resultSet.getString("address"), resultSet.getString("owner"));
        }
    }
}
