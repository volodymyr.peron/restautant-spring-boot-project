package com.softserveinc.shopping.controller;

import com.softserveinc.shopping.model.Dish;
import com.softserveinc.shopping.service.DishService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Slf4j
@RestController
@RequestMapping("/dishes")
public class DishController {

    private final DishService dishService;

    @Autowired
    public DishController(DishService dishService) {
        this.dishService = dishService;
    }

    // commented code because Spring Data REST gives us a implemented CRUD api's with included hyperlinks.

//    @GetMapping
//    public CollectionModel<EntityModel<Dish>> getDishes() {
//        log.info("Getting Dishes...");
//        List<Dish> dishes = dishService.getDishes();
//        List<EntityModel<Dish>> dishesWithLinks = new LinkedList<>();
//        for (Dish dish : dishes) {
//            Link dishLink = linkTo(methodOn(DishController.class).getById(dish.getId())).withSelfRel();
//            dishesWithLinks.add(EntityModel.of(dish, dishLink));
//        }
//        return CollectionModel.of(dishesWithLinks, linkTo(methodOn(DishController.class).getDishes()).withSelfRel());
//    }


//    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED)
//    public Dish save(@RequestBody Dish dish) {
//        return dishService.save(dish);
//    }

//    @GetMapping("/{id}")
//    public EntityModel<Dish> getById(@PathVariable Integer id) {
//        Dish dish = dishService.getById(id);
//        return EntityModel.of(dish, linkTo(methodOn(DishController.class).getById(dish.getId())).withSelfRel()
//                /*.andAffordance(afford(linkTo(methodOn(DishController.class).deleteDish(dish.getId()))))*/);
//    }

    @GetMapping("/withComponents")
    public List<Dish> getDishesByNameWithComponents(@RequestParam("name") String name) {
        return dishService.getDishesByNameWithComponents(name);
    }

    @GetMapping("/name/notNull")
    public List<Dish> getWhereNameIsNotNullPaginated() {
        return dishService.getWhereNameIsNotNullPaginated();
    }

//    @PutMapping
//    public Dish update(@RequestBody Dish dish) {
//        return dishService.update(dish);
//    }
//
//    @DeleteMapping("/{id}")
//    public ResponseEntity<Dish> deleteDish(@PathVariable Integer id) {
//        dishService.deleteById(id);
//        return ResponseEntity.noContent().build();
//    }
}
