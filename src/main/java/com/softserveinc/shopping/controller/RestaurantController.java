package com.softserveinc.shopping.controller;

import com.softserveinc.shopping.service.RestaurantService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/restaurants")
public class RestaurantController {

    private final RestaurantService restaurantService;

    @Autowired
    public RestaurantController(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }

    @GetMapping("/count")
    public Integer getRestaurantsCount() {
        return restaurantService.getRestaurantsCount();
    }

    @GetMapping("name/{id}")
    public String getRestaurantNameById(@PathVariable Integer id) {
        return restaurantService.getRestaurantNameById(id);
    }
}
