package com.softserveinc.shopping;

import com.softserveinc.shopping.integration.IntegrationGateway;
import com.softserveinc.shopping.model.Component;
import com.softserveinc.shopping.model.Dish;
import com.softserveinc.shopping.model.Restaurant;
import com.softserveinc.shopping.model.User;
import com.softserveinc.shopping.repository.UserRepository;
import com.softserveinc.shopping.service.DishService;
import com.softserveinc.shopping.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.util.Set;

@SpringBootApplication
@EnableAspectJAutoProxy
public class ShoppingApplication implements CommandLineRunner {

    private final RestaurantService restaurantService;
    //    private final MessageProducer messageProducer;
    private final DishService dishService;
    private final IntegrationGateway integrationGateway;
    private final UserRepository userRepository;

    @Autowired
    public ShoppingApplication(RestaurantService restaurantService, /*MessageProducer messageProducer,*/ DishService dishService, IntegrationGateway integrationGateway, UserRepository userRepository) {
        this.restaurantService = restaurantService;
//        this.messageProducer = messageProducer;
        this.dishService = dishService;
        this.integrationGateway = integrationGateway;
        this.userRepository = userRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(ShoppingApplication.class, args);
    }

    @Override
    public void run(String... args) {
        restaurantService.getRestaurant(new Restaurant(1, "Name 1", "Address 1", "Owner 1"));
        restaurantService.getRestaurantsCount();
        restaurantService.getRestaurantNameById(3);

        Dish sandwich = new Dish();
        sandwich.setName("Sandwich");
        Component cheese = new Component();
        cheese.setName("Cheese");
        cheese.setPrice(25);
        Component meat = new Component();
        meat.setName("Meat");
        meat.setPrice(50);
        Component bread = new Component();
        bread.setName("Bread");
        bread.setPrice(10);
        sandwich.setComponents(Set.of(bread, meat, cheese));

        Dish dish2 = new Dish();
        dish2.setName("dish2");
        Component component2 = new Component();
        component2.setName("component2");
        component2.setPrice(44);
        dish2.setComponents(Set.of(component2));


        dishService.save(sandwich);
        dishService.save(dish2);

        // Code below throws LazyInitializationException because of absence of @Transactional
        // dishRepository.getDishById(1).getComponents().forEach(c -> System.out.println(c.getName()));

        // kafka message send
        // messageProducer.sendMessage("Message from the shopping app");

        // spring integration
        System.out.println(integrationGateway.uppercase("test Data"));

        // spring reactor
        Flux<String> fruitFlux = Flux.just("Apple", "Orange", "Grape", "Banana", "Strawberry");
        fruitFlux.subscribe(f -> System.out.println("Here is some fruit: " + f));

        Flux.just("apple", "orange", "banana", "kiwi", "strawberry")
                .buffer(3)
                .flatMap(x ->
                        Flux.fromIterable(x)
                                .map(String::toUpperCase)
                                .subscribeOn(Schedulers.parallel())
                                .log()
                ).subscribe();


        // data to test reactive api
        User user = new User(1L, "user1", "pwd1");
        User user2 = new User(2L, "user2", "pwd2");
        userRepository.save(user);
        userRepository.save(user2);

    }
}
