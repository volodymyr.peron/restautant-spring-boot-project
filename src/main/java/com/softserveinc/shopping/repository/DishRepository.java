package com.softserveinc.shopping.repository;

import com.softserveinc.shopping.model.Dish;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DishRepository extends JpaRepository<Dish, Integer> {

    //attributePaths can be more complicated : attributePaths = {"attr1.attr1_1.attr1_1_1", "attr2.attr2_2.attr2_2_2"})
    @EntityGraph(value = "Dish.components", attributePaths = {"components"})
    @Query(value = "select d from Dish d where d.name = :name")
    List<Dish> getDishesByNameWithComponents(@Param("name") String name);
    List<Dish> findByNameIsNotNull(Pageable pageable);
}
