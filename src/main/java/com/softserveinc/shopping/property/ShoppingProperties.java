package com.softserveinc.shopping.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "shopping")
@Component
public class ShoppingProperties {
    // by default 10 otherwise pulled from properties file
    private Integer paginationSize = 10;
}
