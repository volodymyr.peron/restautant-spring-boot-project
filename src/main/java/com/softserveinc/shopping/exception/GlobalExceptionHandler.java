package com.softserveinc.shopping.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(DishNotFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Dish was not found")
    public void dishNotFoundExceptionHandler() {
    }
}
