package com.softserveinc.shopping.kafka;

import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Profile("dev")
@Service
public class MessageConsumer {
    @KafkaListener(topics = "${kafka.topic}", groupId = "${kafka.groupId}", containerFactory = "kafkaListenerContainerFactory")
    public void listenGroupBar(String message) {
        System.out.println("Received message : " + message);
    }
}