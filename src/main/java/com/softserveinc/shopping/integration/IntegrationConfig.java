package com.softserveinc.shopping.integration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;

@Configuration
public class IntegrationConfig {

    @Bean
    public IntegrationFlow uppercaseFlow() {
        return IntegrationFlows
                .from("inChannel")                      // input chanel
                .<String, String>transform(String::toUpperCase)             // transformer
                .channel("outChanel")                                       // pass transformed value to outChanel
                .get();

    }

}
