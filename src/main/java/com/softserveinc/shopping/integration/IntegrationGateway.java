package com.softserveinc.shopping.integration;

import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.stereotype.Component;

@Component
@MessagingGateway(defaultRequestChannel="inChannel", defaultReplyChannel = "outChanel")
public interface IntegrationGateway {
    String uppercase(String data);
}
