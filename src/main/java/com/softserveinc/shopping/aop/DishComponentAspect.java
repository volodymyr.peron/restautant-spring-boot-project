package com.softserveinc.shopping.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class DishComponentAspect {

    @Pointcut("execution(public * com.softserveinc.shopping.controller.DishController.get*(..))")
    public void filterComponentPrice() {
    }

    @Before("filterComponentPrice()")
    public void beforeFilterComponentPrice(JoinPoint joinPoint) {
        System.out.println("Before " + joinPoint.toString());
    }

    @Around("filterComponentPrice()")
    public Object aroundFilterComponentPrice(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("Around " + proceedingJoinPoint.toString());
        Object result = proceedingJoinPoint.proceed();
        return result;
    }
}
