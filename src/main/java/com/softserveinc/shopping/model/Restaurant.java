package com.softserveinc.shopping.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Restaurant {
    private Integer id;
    private String name;
    private String address;
    private String owner;
}
